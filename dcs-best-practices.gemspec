# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'dcs_best_practices/version'

Gem::Specification.new do |spec|
  spec.name          = 'dcs-best-practices'
  spec.version       = DCSBestPractices::VERSION
  spec.authors       = %w[DarkWater]
  spec.email         = %w[denis.arushanov@darkcreative.ru]

  spec.summary       = 'Simple best practices, used for soft deployment with capistrano to some servers'
  spec.description   = 'Contains deploy configuration'
  spec.homepage      = 'https://bitbucket.org/DCStudio/best-practices'

  raise 'RubyGems 2.5.1 or newer is required to protect against public gem pushes.' unless spec.respond_to?(:metadata)

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(/^(test|spec|features)/) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(/^exe/) { |f| File.basename(f) }
  spec.require_paths = %w[lib]

  spec.required_ruby_version = '>= 2.5'

  spec.add_development_dependency 'bundler', '~> 1.17.3', '>= 1.17.3'
  spec.add_development_dependency 'rake', '~> 12.0', '>= 12.0.0'

  spec.add_development_dependency 'redcarpet', '~> 3.4', '>= 3.5.0'
  spec.add_development_dependency 'yard', '~> 0.7', '>= 0.7.5'

  spec.add_dependency 'airbrussh', '~> 1.3', '>= 1.3.0'
  spec.add_dependency 'capistrano', '~> 3.10', '>= 3.10.0'
  spec.add_dependency 'capistrano-rails', '~> 1.4', '>= 1.4.0'
  spec.add_dependency 'capistrano-rails-console', '~> 2.3', '>= 2.3.0'
  spec.add_dependency 'capistrano-rvm', '~> 0.1', '>= 0.1.0'
  spec.add_dependency 'capistrano-sidekiq', '~> 1.0', '>= 1.0.0'
  spec.add_dependency 'capistrano3-puma', '~> 4.0', '>= 4.0.0'
  spec.add_dependency 'figaro', '~> 1.1', '>= 1.1.1'
  spec.add_dependency 'puma', '~> 4.3', '>= 4.3.0'

  spec.add_development_dependency 'rubocop', '~> 0.7', '>= 0.7.0'
  spec.add_development_dependency 'rubocop-performance', '~> 1.5', '>= 1.5.0'
  spec.add_development_dependency 'solargraph', '~> 0.35', '>= 0.35.0'
end

# frozen_string_literal: true

require 'capistrano/dcs/defaults'
require 'capistrano/dcs/assets'
require 'capistrano/dcs/remote'
require 'capistrano/dcs/ssh'

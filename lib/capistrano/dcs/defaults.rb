# frozen_string_literal: true

namespace :load do
  task :defaults do
    lock '>= 3.10.0'
    load 'config/deploy.rb'
    load "config/deploy/#{fetch(:stage)}.rb" if fetch(:stage)

    set :deploy_to,             "/home/#{fetch(:user)}/projects/#{fetch(:application)}"
    set :bundle_path,           nil
    set :bundle_binstubs,       nil
    set :bundle_flags,          '--system'
    set :bundle_without,        %i[development test console doc]
    set :use_sudo,              false

    ask :branch,                proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

    set :pty,                   false

    set :log_level,             :info
    set :format_options,        truncate: false
    set :keep_releases,         5
    set :env, worker_count:     8

    set :linked_files,          fetch(:linked_files, []) + %w[config/application.yml public/sitemap.xml]
    set :linked_dirs,           fetch(:linked_dirs, []) + %w[bin log tmp/cache tmp/pids tmp/sockets vendor/bundle
                                                             public/system public/uploads public/mails .bundle]

    set :default_environments,  'RAILS_ENV=production'
    set :production_rake,       "rvm use #{fetch(:rvm_ruby_version)} do bundle exec rake #{fetch(:default_environments)}"
    set :rake,                  "rvm use #{fetch(:rvm_ruby_version)} do bundle exec rake"
    set :bundle_cmd,            "rvm use #{fetch(:rvm_ruby_version)} do bundle"

    set :assets_roles,          %i[web app db]
  end
end
